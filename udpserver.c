#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<unistd.h>
#include<netinet/in.h>
#include<arpa/inet.h>
void main()
{
char *ip="127.0.0.1";
int port=5555;
int sockfd;
struct sockaddr_in seradd,cliadd;
char buf[1024];
socklen_t addr_size;
sockfd=socket(AF_INET,SOCK_DGRAM,0);
if(sockfd<0)
{
perror("err in socket creation\n");
exit(1);
}
printf("socket created successfully\n");
memset(&seradd,'\0',sizeof(seradd));
seradd.sin_family=AF_INET;
seradd.sin_port=htons(port);
seradd.sin_addr.s_addr=inet_addr(ip);
int n=bind(sockfd,(struct sockaddr*)&seradd,sizeof(seradd));
if(n<0)
{
perror("bind failed\n");
exit(1);
}
printf("socket binded successfully with port %d\n",port);
addr_size=sizeof(cliadd);
bzero(buf,1024);
recvfrom(sockfd,buf,sizeof(buf),0,(struct sockaddr*)&cliadd,&addr_size);
printf("client: %s\n",buf);
bzero(buf,1024);
strcpy(buf,"Welcome client");
printf("server: %s\n",buf);
sendto(sockfd,buf,strlen(buf),0,(struct sockaddr*)&cliadd,addr_size);
}
