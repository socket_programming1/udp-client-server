#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<unistd.h>
#include<netinet/in.h>
#include<arpa/inet.h>
void main()
{
char *ip="127.0.0.1";
int port= 5555;
int sockfd;
struct sockaddr_in seraddr;
char buf[1024];
socklen_t addr_size;
sockfd=socket(AF_INET,SOCK_DGRAM,0);
if(sockfd<0)
{
printf("err in socket creation\n");
exit(0);
}
printf("socket created successfully\n");
memset(&seraddr,'\0',sizeof(seraddr));
seraddr.sin_family=AF_INET;
seraddr.sin_port=htons(port);
seraddr.sin_addr.s_addr=inet_addr(ip);
bzero(buf,1024);
strcpy(buf,"Hello server");
addr_size=sizeof(seraddr);
sendto(sockfd,buf,strlen(buf),0,(struct sockaddr*)&seraddr,addr_size);
printf("Client: %s\n",buf);
bzero(buf,1024);
recvfrom(sockfd,buf,sizeof(buf),0,(struct sockaddr*)&seraddr,&addr_size);
printf("server: %s\n",buf);
}
